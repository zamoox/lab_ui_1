﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Compression;

namespace Lab_1
{
    
    public partial class Form1 : Form
    {
        String initialPath = "C:\\Users\\zamoo\\Desktop";
        String[] filePaths;
        String[] fileNames; 
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {

        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = true;
            dialog.InitialDirectory = initialPath;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                filePaths = dialog.FileNames;
                fileNames = dialog.SafeFileNames;
                fileNamesBox.Text = "";
                foreach (String item in filePaths)
                {
                    fileNamesBox.Text += item + '\r' +'\n';
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                locationBox.Text = dialog.SelectedPath;
            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {

            if (fileNamesBox.Text != "" && archiveNameBox.Text != "" && locationBox.Text != "")
            {

                String archivePath = locationBox.Text + "\\" + archiveNameBox.Text + ".zip";
                using (ZipArchive zipArchive = ZipFile.Open(archivePath, ZipArchiveMode.Create))
                {

                    for (int i = 0; i < fileNames.Length; i++)
                    {
                        // путь к добавляемому файлу
                        const string pathFileToAdd = @"D:\\file.txt";
                        // имя добавляемого файла
                        const string nameFileToAdd = "file.txt";
                        // вызов метода для добавления файла в архив
                        zipArchive.CreateEntryFromFile(filePaths[i], fileNames[i]);
                    }

                    status.Text = "Archive created successfully!";
                }
            }

            else status.Text = "Enter all data";
        }

        private void label5_Click(object sender, EventArgs e)
        {
            
        }
    }
}
